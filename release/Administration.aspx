﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Administration.aspx.cs" Inherits="aspnetprojekt.Administration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     
    <asp:Label Text="You are not allowed on this page!" runat="server" ID="NotAdminLabel" CssClass="ValidationErrors" visible="false"></asp:Label>

    <asp:Panel runat="server" ID="adminContent">
        <form id="AdminForm">
       
            <div id="AdminPageMediaDiv">

                <h2>Administer Media</h2>
                <p>
                    <asp:Label runat="server" id="MediaDataChangedLabel" CssClass="AdministerDataChangedLabel"></asp:Label>
                </p>
                <asp:Objectdatasource runat="server" ID="AdminVideoDataSource" SelectMethod="GetSeriesList"
                    TypeName="aspnetprojekt.Code.BusinessLogicLayer.Service" DataObjectTypeName="aspnetprojekt.Code.BusinessLogicLayer.Media"
                    DeleteMethod="DeleteMediaByMedia" InsertMethod="AddMediaByMedia" UpdateMethod="AddMediaByMedia"
                    OnInserted="AdminMediaObjectDataSource_Inserted" OnInserting="AdminMediaObjectDataSource_Inserting"
                    OnUpdated="AdminMediaObjectDataSource_Updated" OnUpdating="AdminMediaObjectDataSource_Updating"
                    OnDeleted="AdminMediaObjectDataSource_Deleted"></asp:Objectdatasource>

                <asp:ObjectDataSource runat="server" ID="VideoTypeDataSource" TypeName="aspnetprojekt.Code.BusinessLogicLayer.Service"
                             SelectMethod="GetVideoTypes" DataObjectTypeName="aspnetprojekt.Code.BusinessLogicLayer.Videotype"></asp:ObjectDataSource>
                <asp:ObjectDataSource runat="server" ID="GenreDataSource" TypeName="aspnetprojekt.Code.BusinessLogicLayer.Service"
                             SelectMethod="GetGenres" DataObjectTypeName="aspnetprojekt.Code.BusinessLogicLayer.Genre"></asp:ObjectDataSource>

                <asp:ValidationSummary ID="AdminMediaValidationSummary" runat="server" CssClass="ValidationErrors" 
                    HeaderText="An error occured when attempting to edit a media, please fix the errors" ValidationGroup="AdminMediaEditValidationInsert"></asp:ValidationSummary>

                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="ValidationErrors" 
                    HeaderText="An error occured when attempting to edit a media, please fix the errors" ValidationGroup="AdminMediaEditValidationUpdate"></asp:ValidationSummary>
         
             <asp:DataPager runat="server" PagedControlID="AdminListViewVideo" PageSize="15">
                     <Fields>
                       <asp:NextPreviousPagerField FirstPageText="&lt;&lt;" ShowFirstPageButton="True" 
                            ShowNextPageButton="False" ShowPreviousPageButton="False" />
                        <asp:NumericPagerField ButtonCount="20" />
                        <asp:NextPreviousPagerField LastPageText="&gt;&gt;" ShowLastPageButton="True" 
                            ShowNextPageButton="False" ShowPreviousPageButton="False" />
                    </Fields>
             </asp:DataPager>
             <asp:listview runat="server" id="AdminListViewVideo" DataSourceID="AdminVideoDataSource" DataKeyNames="MediaID" InsertItemPosition="FirstItem">
                <LayoutTemplate>
                    <table class="grid">
                        <tr>
                            <th>
                                Name
                            </th>
                            <th>
                                Genre
                            </th>
                            <th>
                                Type
                            </th>
                            <th>
                                Episodes
                            </th>
                            <th>
                                Summary
                            </th>
                        </tr>
                        <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:Label ID="NameLabel" runat="server" CssClass="AdministerTableItem" Text='<%# Eval("Name") %>' />
                        </td>
                        <td>
                            <asp:Label ID="GenreLabel" runat="server" CssClass="AdministerTableItem" Text='<%# Eval("Genre.GetGenre") %>' />
                        </td>
                        <td>
                            <asp:Label ID="TypeLabel" runat="server" CssClass="AdministerTableItem" Text='<%# Eval("Type.Type") %>' />
                        </td>

                        <td>
                            <asp:Label ID="EpisodesLabel" runat="server"  CssClass="AdministerTableItem" Text='<%# Eval("Episodes") %>' />
                        </td>
                        <td>
                            <asp:Label ID="SummaryLabel" runat="server" CssClass="AdministerTableItem" Text='<%# Eval("Summary") %>' />
                        </td>
                        <td class="command">
                            <asp:LinkButton ID="EditMediaLinkButton" runat="server" CommandName="Edit" Text="Edit" CausesValidation="false" />
                            <asp:LinkButton ID="DeleteMediaLinkButton" runat="server" CommandName="Delete" Text="Delete" CausesValidation="false" 
                                OnClientClick='<%# String.Format("return confirm(\"Do you want to delete &rsquo;{0}&rsquo; permanently?\" )", Eval("Name")) %>' />
                        </td>
                    </tr>
                </ItemTemplate>
                <EmptyDataTemplate>
                    <table>
                        <tr>
                            <td>
                                Media Data is missing
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <InsertItemTemplate>
                    <tr>
                        <td>
                            <asp:TextBox ID="AddMediaNameTextBox" runat="server" Text='<%# Bind("Name") %>' />
                            <asp:RequiredFieldValidator ID="AddMediaNameRequiredFieldValidation" runat="server" ErrorMessage="You must type a media name!"
                                Text="*" ControlToValidate="AddMediaNameTextBox" Display="Dynamic" CssClass="ValidationErrors" ValidationGroup="AdminMediaEditValidationInsert" />
                        </td>
                        <td>
                            <asp:DropDownList ID="AddMediaGenreTextBox" runat="server" DataSourceID="GenreDataSource" 
                                SelectedValue='<%# Bind("GetGenreID") %>' DataTextField='GetGenre' DataValueField="GenreID"></asp:DropDownList>
                        </td>
                        <td> 
                            <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="VideoTypeDataSource" 
                                SelectedValue='<%# Bind("GetVideoTypeID") %>' DataTextField='Type' DataValueField="TypeID"></asp:DropDownList>                            
                        </td>
                        <td>
                            <asp:TextBox CssClass="AddMediaEpisodesTextBox" ID="AddMediaEpisodesTextBox" runat="server" Text='<%# Bind("Episodes") %>' />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="You must type the number of episodes"
                                Text="*" ControlToValidate="AddMediaEpisodesTextBox" Display="Dynamic" CssClass="ValidationErrors" ValidationGroup="AdminMediaEditValidationInsert" />
                        </td>
                        <td>
                            <asp:TextBox CssClass="AddMediaSummaryTextBox" ID="AddMediaSummaryTextBox" runat="server" Text='<%# Bind("Summary") %>' />
                        </td>
                        <td>
                            <asp:LinkButton ID="InsertLinkButton" runat="server" CommandName="Insert" Text="Add" ValidationGroup="AdminMediaEditValidationInsert" CausesValidation="true"/>
                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Cancel" Text="Clear" CausesValidation="false"/>
                        </td>
                    </tr>
                </InsertItemTemplate>
                <EditItemTemplate>
                    <tr>
                        <td>
                            <asp:TextBox ID="AddMediaNameTextBox" runat="server" Text='<%# Bind("Name") %>' />
                            <asp:RequiredFieldValidator ID="AddMediaNameRequiredFieldValidation" runat="server" ErrorMessage="You must type a media name!"
                                Text="*" ControlToValidate="AddMediaNameTextBox" Display="Dynamic" CssClass="ValidationErrors" ValidationGroup="AdminMediaEditValidationUpdate" />
                        </td>
                        <td>
                            <asp:DropDownList ID="EditMediaGenreTextBox" runat="server"  DataSourceID="GenreDataSource" 
                                SelectedValue='<%# Bind("GetGenreID") %>' DataTextField='GetGenre' DataValueField="GenreID"></asp:DropDownList>
                        </td>
                        <td> 
                              <asp:DropDownList ID="EditMediaVideoTextBox" runat="server" DataSourceID="VideoTypeDataSource"
                               SelectedValue='<%# Bind("GetVideoTypeID") %>' DataTextField='Type' DataValueField="TypeID"  ></asp:DropDownList>    
                        </td>
                        <td>
                            <asp:TextBox ID="AddMediaEpisodesTextBox" CssClass="AddMediaEpisodesTextBox" runat="server" Text='<%# Bind("Episodes") %>' />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="You must type the number of episodes"
                                Text="*" ControlToValidate="AddMediaEpisodesTextBox" Display="Dynamic" CssClass="ValidationErrors" ValidationGroup="AdminMediaEditValidationUpdate" />
                        </td>
                        <td>
                            <asp:TextBox ID="AddMediaSummaryTextBox" CssClass="AddMediaSummaryTextBox" runat="server" Text='<%# Bind("Summary") %>' />
                        </td>
                        <td>
                            <asp:LinkButton ID="InsertLinkButton" runat="server" CommandName="Update" Text="Apply" ValidationGroup="AdminMediaEditValidationUpdate" CausesValidation="true"/>
                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Cancel" Text="Cancel" CausesValidation="false"/>
                        </td>
                    </tr>
                </EditItemTemplate>
                </asp:listview>
            </div>
        
            <h2>Administer Accounts</h2>
            <p>
                <asp:Label runat="server" id="AccountDataChangedLabel" CssClass="AdministerDataChangedLabel"></asp:Label>
            </p>
            <asp:DataPager ID="MemberEditDataPager" runat="server" PagedControlID="AdminListViewUsers" PageSize="15">
                     <Fields>
                       <asp:NextPreviousPagerField FirstPageText="&lt;&lt;" ShowFirstPageButton="True" 
                            ShowNextPageButton="False" ShowPreviousPageButton="False" />
                        <asp:NumericPagerField ButtonCount="20" />
                        <asp:NextPreviousPagerField LastPageText="&gt;&gt;" ShowLastPageButton="True" 
                            ShowNextPageButton="False" ShowPreviousPageButton="False" />
                    </Fields>
             </asp:DataPager>
            <div id="AdminPageUsersDiv">
                <asp:Objectdatasource runat="server" ID="AdminUsersDataSource" SelectMethod="GetMemberList"
                    TypeName="aspnetprojekt.Code.BusinessLogicLayer.Service" DataObjectTypeName="aspnetprojekt.Code.BusinessLogicLayer.Member"
                    DeleteMethod="DeleteMemberByMember" UpdateMethod="EditMemberByMember"
                    OnUpdated="AdminMemberObjectDataSource_Updated" OnUpdating="AdminMemberObjectDataSource_Updating"
                    OnDeleted="AdminMemberObjectDataSource_Deleted"></asp:Objectdatasource>


                <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="ValidationErrors" 
                    HeaderText="An error occured when attempting to edit a media, please fix the errors" ValidationGroup="AdminMemberEditValidationUpdate"></asp:ValidationSummary>


                <asp:listView runat="server" ID="AdminListViewUsers" DataSourceID="AdminUsersDataSource" DataKeyNames="MemberID">
               <LayoutTemplate>
                    <table class="grid">
                        <tr>
                            <th>
                                Username
                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                Nationality
                            </th>
                            <th>
                                
                            </th>
                        </tr>
                        <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:Label ID="MemberUserNameLabel" runat="server" Text='<%# Eval("Username") %>' />
                        </td>
                        <td>
                            <asp:Label ID="MemberNameLabel" runat="server" Text='<%# Eval("Name") %>' />
                        </td>

                        <td>
                            <asp:Label ID="MemberEmailLabel" runat="server" Text='<%# Eval("Email") %>' />
                        </td>
                        <td>
                            <asp:Label ID="MemberNationalityLabel" runat="server" Text='<%# Eval("Nationality") %>' />
                        </td>
                        <td class="command">
                            <asp:LinkButton ID="EditMediaLinkButton" runat="server" CommandName="Edit" Text="Edit" CausesValidation="false" />
                            <asp:LinkButton ID="DeleteMediaLinkButton" runat="server" CommandName="Delete" Text="Delete" CausesValidation="false" 
                                OnClientClick='<%# String.Format("return confirm(\"Do you want to delete &rsquo;{0}&rsquo; permanently?\" )", Eval("Name")) %>' />
                        </td>
                    </tr>
                </ItemTemplate>
                <EmptyDataTemplate>
                    <table>
                        <tr>
                            <td>
                                Media Data is missing
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <EditItemTemplate>
                    <tr>
                        <td>
                            <asp:Label ID="AddMemberUsernameTextBox" runat="server" Text='<%# Bind("Username") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="AddMemberNameTextBox" runat="server" Text='<%# Bind("Name") %>' />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="The member must have a name!"
                                Text="*" ControlToValidate="AddMemberNameTextBox" Display="Dynamic" CssClass="ValidationErrors" ValidationGroup="AdminMemberEditValidationUpdate" />
                        </td>
                        <td>
                            <asp:TextBox ID="AddMemberEmailTextBox" runat="server" Text='<%# Bind("Email") %>' />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="The member must have an email!"
                                Text="*" ControlToValidate="AddMemberEmailTextBox" Display="Dynamic" CssClass="ValidationErrors" ValidationGroup="AdminMemberEditValidationUpdate" />
                            <asp:RegularExpressionValidator ID="EmailRegularExpressionValidator" runat="server" ErrorMessage="EmailAddressen verkar inte vara korrekt."
                                    ControlToValidate="AddMemberEmailTextBox" Display="Dynamic" ValidationExpression="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$"
                                    CssClass="field-validation-error" ValidationGroup="AdminMemberEditValidationUpdate"></asp:RegularExpressionValidator>
                        </td>
                        <td>
                            <asp:TextBox ID="AddMemberNationalityTextBox" runat="server" Text='<%# Bind("Nationality") %>' />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="The member must have a nationality!"
                                Text="*" ControlToValidate="AddMemberNationalityTextBox" Display="Dynamic" CssClass="ValidationErrors" ValidationGroup="AdminMemberEditValidationUpdate" />
                        </td>
                        <td>
                            <asp:LinkButton ID="InsertLinkButton" runat="server" CommandName="Update" Text="Apply" ValidationGroup="AdminMemberEditValidationUpdate" CausesValidation="true"/>
                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Cancel" Text="Cancel" CausesValidation="false"/>
                        </td>
                    </tr>
                </EditItemTemplate> 
                </asp:listView>
            </div> 
        </form>
    </asp:Panel>


</asp:Content>
