﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using aspnetprojekt.Code.BusinessLogicLayer;
using aspnetprojekt.Code;

namespace aspnetprojekt
{
    public partial class Administration : System.Web.UI.Page
    {
        Service service;

        private new Site site { get { return base.Master as Site; } }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!site.IfAdmin())
            {
                Redirect();
            }
        }

        //Media edit
        protected void AdminMediaObjectDataSource_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception != null)
            {
                PageExtensions.AddErrorMessage(this, "An error occured when attempting to add media.", "AdminMediaEditValidationInsert");

                e.ExceptionHandled = true;
            }
            else
            {
                MediaDataChangedLabel.Visible = true;
                MediaDataChangedLabel.Text = "Media has been added.";
                AccountDataChangedLabel.Visible = false;
            }
        }

        protected void AdminMediaObjectDataSource_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
        {
            Media media = (Media)e.InputParameters[0];

            if (!media.IsValid)
            {
                e.Cancel = true;
            }
        }

        protected void AdminMediaObjectDataSource_Updated(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception != null)
            {
                PageExtensions.AddErrorMessage(this, "An error occured when attempting to edit media.", "AdminMediaEditValidationUpdate");

                e.ExceptionHandled = true;
            }
            else
            {
                MediaDataChangedLabel.Visible = true;
                MediaDataChangedLabel.Text = "Media has been edited.";
                AccountDataChangedLabel.Visible = false;
            }
        }

        protected void AdminMediaObjectDataSource_Updating(object sender, ObjectDataSourceMethodEventArgs e)
        {
            Media media = (Media)e.InputParameters[0];

            if (!media.IsValid)
            {
                e.Cancel = true;
            }
        }

        protected void AdminMediaObjectDataSource_Deleted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception != null)
            {
                PageExtensions.AddErrorMessage(this, "An error occured when attempting to delete media.", "AdminMediaEditValidationInsert");

                e.ExceptionHandled = true;
            }
            else
            {
                MediaDataChangedLabel.Visible = true;
                MediaDataChangedLabel.Text = "The media has been deleted.";
                AccountDataChangedLabel.Visible = false;
            }
        }



        //MemberEdit
        protected void AdminMemberObjectDataSource_Updated(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception != null)
            {
                PageExtensions.AddErrorMessage(this, "An error occured when attempting to edit media.", "AdminMemberEditValidationUpdate");

                e.ExceptionHandled = true;
            }
            else
            {
                AccountDataChangedLabel.Visible = true;
                AccountDataChangedLabel.Text = "Member has been edited.";
                MediaDataChangedLabel.Visible = false;
            }
        }

        protected void AdminMemberObjectDataSource_Updating(object sender, ObjectDataSourceMethodEventArgs e)
        {
            var member = (Member)e.InputParameters[0];
            if (!member.IsValid)
            {
                e.Cancel = true;
            }
        }

        protected void AdminMemberObjectDataSource_Deleted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception != null)
            {
                PageExtensions.AddErrorMessage(this, "An error occured when attempting to delete member.", "AdminMemberEditValidationUpdate");

                e.ExceptionHandled = true;
            }
            else
            {
                AccountDataChangedLabel.Visible = true;
                AccountDataChangedLabel.Text = "Member has been deleted.";
                MediaDataChangedLabel.Visible = false;
            }
              
        }


        private void Redirect()
        {
            //var myScript = "\n<script type=\"text/javascript\" language=\"Javascript\" id=\"EventScriptBlock\">\n";
            //myScript += "alert('You are not allowed to enter this page!');";
            //myScript += "\n\n </script>";
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);

            //Response.Redirect("~/Default.aspx");

            NotAdminLabel.Visible = true;
            adminContent.Enabled = false;
            adminContent.Visible = false;
        }







        /*

        public void GetGenres1(Media ge)
        {

            var test = 0;

            DropDownList cb = (DropDownList)AdminListViewVideo.FindControl("EditMediaGenreTextBox");
            foreach (ListItem bm in cb.Items)
            {
                if (bm.Selected)
                {
                    test = int.Parse(bm.Value);

                }
            }
            try
            {
                Media m = new Media();

                    ge.GenreID = test;
                       (ge, test);
                    Page.SetTempData("Message", "Bilannonsen har uppdaterats.");
                    Response.RedirectToRoute("CarAdDetails", new { id = ge.CarAdID });
                    Context.ApplicationInstance.CompleteRequest();


                
            }
            catch (Exception)
            {
                ModelState.AddModelError(String.Empty, "Fel inträffade då bilannonsen skulle uppdateras.");
            }
        }*/
    }
}