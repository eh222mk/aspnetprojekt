﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using aspnetprojekt.Code.BusinessLogicLayer;

namespace aspnetprojekt
{
    public partial class Series : System.Web.UI.Page
    {
        private const string NavigateQueryString = "series";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string navigatedSeries = Request.QueryString[NavigateQueryString];

                Service service = new Service();

                Media CurrentMedia = service.GetMediaByName(navigatedSeries);

                if (CurrentMedia.Summary != null)
                {
                    SummaryLabel.Text = CurrentMedia.Summary.ToString();
                }
                else
                {
                    SummaryLabel.Text = "No Summary available";
                }

                SummaryLabel.Visible = true;
                GenreLabel.Text = CurrentMedia.Genre.GetGenre;
                GenreLabel.Visible = true;

                TypeLabel.Text = CurrentMedia.Type.ToString();
                TypeLabel.Visible = true;

                for (int i = 1; i <= CurrentMedia.Episodes; i++)
                {
                    HyperLink episodeLink = new HyperLink();
                    episodeLink.ID = "episodeLink" + i;
                    episodeLink.Text = "Episode" + i;
                    episodeLink.CssClass = "EpisodeLink";
                    episodeLink.NavigateUrl = "~/Episode.aspx?series=" + navigatedSeries + "&episode=" + i;
                    Page.Controls.Add(episodeLink);
                }
            }
            catch
            {
                TitleLabel.Text = "A series has not been chosen";
                TitleLabel.Visible = true;
                SummaryLabel.Visible = false;
                GenreLabel.Visible = false;
            }
        }
    }
}