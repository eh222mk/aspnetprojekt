﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using aspnetprojekt.Code.BusinessLogicLayer;

namespace aspnetprojekt
{
    public partial class Episode : System.Web.UI.Page
    {
        public new Site site { get { return base.Master as Site; } }

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                
                string series = Request.QueryString["series"];
                string episode = Request.QueryString["episode"];
                int intepisode = int.Parse(episode);


                try
                {
                    string username = site.GetSessionUsername().ToString();
                }
                catch 
                {
                    if (intepisode > 3)
                    {
                        Panel1.Visible = true;
                        Panel1.Enabled = true;

                        Panel2.Visible = false;
                        Panel2.Enabled = false;
                    }
                    else
                    {
                        Panel1.Visible = false;
                        Panel1.Enabled = false;

                        Panel2.Visible = true;
                        Panel2.Visible = true;
                    }
                }


                Service service = new Service();

                int amountOfEpisodesInSeries = service.getAmountOfEpisodesBySeries(series);

                if (intepisode > 0 && intepisode < amountOfEpisodesInSeries)
                {
                    HeaderLabel.Text = series + " Episode " + episode;
                }
                else
                {
                    HeaderLabel.Text = "Invalid Episode for the series";
                }

                //Back & forward buttons
                BackToSeries.Text = "Back to episode list";
                BackToSeries.NavigateUrl = "~/Series.aspx?series=" + series;

                ForwardOneEpisode.Text = "Go to episode " + (intepisode + 1);
                ForwardOneEpisode.NavigateUrl = "~/Episode.aspx?series=" + series + "&episode=" + (intepisode + 1);

                BackOneEpisode.Text = "Go to episode " + (intepisode - 1);
                BackOneEpisode.NavigateUrl = "~/Episode.aspx?series=" + series + "&episode=" + (intepisode - 1);

                if (intepisode == amountOfEpisodesInSeries)
                {
                    ForwardOneEpisode.Visible = false;
                }
                if (intepisode == 1)
                {
                    BackOneEpisode.Visible = false;
                }
            }
            catch
            {
                HeaderLabel.Text = "Invalid Episode!";
                Panel2.Visible = false;
                Panel2.Enabled = false;
            }
        }
    }
}