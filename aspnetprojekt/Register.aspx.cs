﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using aspnetprojekt.Code.BusinessLogicLayer;
using aspnetprojekt.Code;

namespace aspnetprojekt
{
    public partial class Register : System.Web.UI.Page
    {
        private const string validationGroup = "RegisterValidation";

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void RegisterButton_Click(object sender, EventArgs e)
        {
            try
            {
                string username = UsernameTextBox.Text;
                string password = PasswordTextBox.Text;
                string repeatPassword = RepeatPasswordTextBox.Text;
                string name = FullNameTextBox.Text;
                string email = EmailTextBox.Text;
                string nationality = NationalityTextBox.Text;

                if(repeatPassword != password)
                {
                    throw new Exception("Passwords doesn't match!");
                }

                Member member = new Member(username, password, name, email, nationality);

                if (member.IsValid)
                {
                    Service service = new Service();

                    service.RegisterMember(member);

                    RegisterCompletedLabel.Text = "Register succeded! You will be redirected shortly.";

                    Response.AddHeader("REFRESH", "3;URL=Default.aspx");
                }
                else
                {
                    PageExtensions.AddErrorMessage(this, "Credentials are not valid.", validationGroup);
                }
            }
            catch(Exception exc)
            {
                // "Seems like something went wrong when trying to register user"
                PageExtensions.AddErrorMessage(Page, exc.Message, validationGroup);
            }
        }
    }
}