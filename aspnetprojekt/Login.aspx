﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="aspnetprojekt.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div class="Login">
    <div id="loginDiv">
        <h2>Login</h2>
        <asp:PlaceHolder ID="PlaceHolderLogin" runat="server">
        <asp:ValidationSummary ID="ValidationSummary1" CssClass="ValidationErrors" runat="server" ValidationGroup="loginvalidation" />
            <div class="left">
            <asp:Label ID="UserLabel" runat="server" Text="Label" CssClass="loginUserLabel">Username</asp:Label>
            </div>
            <div class="right">
            <asp:TextBox ID="UserTextBox" runat="server" MaxLength="50" CssClass="loginUserTextBox" CausesValidation="True"></asp:TextBox>
            </div>
            <asp:RequiredFieldValidator ID="RequiedFieldValidator1" runat="server" Display="Dynamic" ErrorMessage="You must type in a username"
            ValidationGroup="loginvalidation" ControlToValidate="UserTextBox">*</asp:RequiredFieldValidator>
            <div class="left">
            <asp:Label ID="PasswordLabel" runat="server" Text="Label" CssClass="loginPasswordLabel">Password</asp:Label>
            </div>
            <div class="right">
            <asp:TextBox ID="PasswordTextBox" runat="server" MaxLength="20" CssClass="loginPasswordTextBox" TextMode="Password" CausesValidation="True" ToolTip="Password"></asp:TextBox>
            </div>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Display="Dynamic" ErrorMessage="You must type in a password"
            ValidationGroup="loginvalidation" ControlToValidate="PasswordTextBox">*</asp:RequiredFieldValidator> 
            <div class="left">
            <asp:Button ID="LoginButton" runat="server" OnClick="LoginButton_Click" Text="Log in" 
            ValidationGroup="loginvalidation" CausesValidation="true" CssClass="loginButton"></asp:Button>
            </div>
        
        </asp:PlaceHolder>        
    </div>
</div>

</asp:Content>
