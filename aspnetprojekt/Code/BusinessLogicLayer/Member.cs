﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace aspnetprojekt.Code.BusinessLogicLayer
{
    public class Member : BusinessObjectBase
    {
        private string _Username;
        private string _Password;
        private string _Name;
        private string _Email;
        private string _Nationality;
        private string _status;

        public int MemberID
        {
            get;
            set;
        }

        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        public string Username
        {
            get
            {
                return _Username;
            }
            set
            {
                this.ValidationErrors.Remove("Username");

                if (String.IsNullOrWhiteSpace(value))
                {
                    this.ValidationErrors.Add("Username", "You must write a username.");
                }
                else if (value.Length > 25)
                {
                    this.ValidationErrors.Add("Username", "The username can be maximum 25 characters");
                }

                this._Username = value != null ? value.Trim() : null;
            }
        }
        public string Password
        {
            get
            {
                return _Password;
            }
            set
            {
                this.ValidationErrors.Remove("Password");

                if (String.IsNullOrWhiteSpace(value))
                {
                    this.ValidationErrors.Add("Password", "You must write a password");
                }
                else if (value.Length > 50)
                {
                    this.ValidationErrors.Add("Password", "The Password can be maximum 50 characters");
                }

                this._Password = value != null ? value.Trim() : null;
            }
        }
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                this.ValidationErrors.Remove("Name");

                if (String.IsNullOrWhiteSpace(value))
                {
                    this.ValidationErrors.Add("Name", "You muste write a name.");
                }
                else if (value.Length > 40)
                {
                    this.ValidationErrors.Add("Name", "The name can be maximum 40 characters");
                }

                this._Name = value != null ? value.Trim() : null;
            }
        }
        public string Email
        {
            get
            {
                return _Email;
            }
            set
            {
                this.ValidationErrors.Remove("Email");

                if (String.IsNullOrWhiteSpace(value))
                {
                    this.ValidationErrors.Add("Email", "You must write an email.");
                }
                else if (value.Length > 50)
                {
                    this.ValidationErrors.Add("Email", "The email can be maximum 50 characters");
                }

                this._Email = value != null ? value.Trim() : null;
            }
        }
        public string Nationality
        {
            get
            {
                return _Nationality;
            }
            set
            {
                this.ValidationErrors.Remove("Nationality");

                if (String.IsNullOrWhiteSpace(value))
                {
                    this.ValidationErrors.Add("Nationality", "You must write a nationality");
                }
                else if (value.Length > 30)
                {
                    this.ValidationErrors.Add("Nationality", "The nationality can be maximum 30 characters");
                }

                this._Nationality = value != null ? value.Trim() : null;
            }
        }

        public Member()
        {
        }

        public Member(string username, string password, string name, string email, string nationality)
        {
            this.Username = username;
            this.Password = password;
            this.Name = name;
            this.Email = email;
            this.Nationality = nationality;
        }

        public Member(string username, string password, string name, string email, string nationality, int id)
        {
            this.Username = username;
            this.Password = password;
            this.Name = name;
            this.Email = email;
            this.Nationality = nationality;
            this.MemberID = id;
        }
    }
}