﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace aspnetprojekt.Code.BusinessLogicLayer
{
    public class Genre : BusinessObjectBase
    {
        private string _Genre;

        public int GenreID
        {
            get;
            set;
        }

        public string GetGenre
        {
            get
            {
                return _Genre;
            }
            set
            {
                if (String.IsNullOrWhiteSpace(value))
                {
                    this.ValidationErrors.Add("Username", "You must write a username.");
                }
                _Genre = value != null ? value.Trim() : null; ;
            }
        }

        public Genre()
        {
        }

        public Genre(string genre)
        {
            GetGenre = genre;
        }
        
        public Genre(string genre, int genreID)
        {
            GetGenre = genre;
            GenreID = genreID;
        }

    }
}