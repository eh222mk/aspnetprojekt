﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using aspnetprojekt.Code.DataAccessLayer;

namespace aspnetprojekt.Code.BusinessLogicLayer
{
    public class Service
    {
        private MemberDAL _memberDAL;
        private MediaDAL _mediaDAL;

        MemberDAL MemberDAL
        {
            get
            {
                return this._memberDAL ?? (this._memberDAL = new MemberDAL());
            }
        }

        MediaDAL MediaDAL
        {
            get
            {
                return this._mediaDAL ?? (this._mediaDAL = new MediaDAL());
            }
        }

        public List<Member> GetMemberList()
        {
            List<Member> memberList = this.MemberDAL.GetMemberList();

            if (memberList != null)
            {
                return memberList;
            }
            return null;
        }

        public List<Media> GetSeriesList()
        {
            List<Media> mediaList = this.MediaDAL.GetMediaList();

            if (mediaList != null)
            {
                return mediaList;
            }
            return null;
        }

        public void RegisterMember(Member member)
        {
            if (member.IsValid)
            {
                List<Member> memberList = GetMemberList();
                bool memberExists = false;
                foreach(Member m in memberList)
                {
                    if (m.Username == member.Username)
                    {
                        memberExists = true;
                        break;
                    }
                    
                }
                if (!memberExists)
                {
                    this.MemberDAL.RegisterMember(member);
                }
                else
                {
                    ApplicationException ex = new ApplicationException("Username already exists");
                    //ex.Data.Add("Member", member);
                    throw ex;
                }
            }
            else
            {
                ApplicationException ex = new ApplicationException(member.Error);
                //ex.Data.Add("Member", member);
                throw ex;
            }
            
        }

        public void DeleteMemberByMember(Member member)
        {
            if (member.IsValid)
            {
                this.MemberDAL.DeleteMember(member);
            }
        }

        public void DeleteMediaByMedia(Media media)
        {
            if (media.IsValid)
            {
                this.MediaDAL.DeleteMediaByMedia(media);
            }
            
        }

        public void AddMediaByMedia(Media media)
        {

            if (media.IsValid)
            {
                if (media.MediaID == 0)
                {
                    this.MediaDAL.AddMediaByMedia(media);
                }
                else
                {
                    this.MediaDAL.UpdateMediaByMedia(media);
                }

            }
            else
            {
                ApplicationException ex = new ApplicationException(media.Error);
                ex.Data.Add("Media", media);
                throw ex;
            }
        }

        public void EditMemberByMember(Member member)
        {
            if (member.IsValid)
            {
                this.MemberDAL.UpdateMemberByMember(member);
            }
            else
            {
                ApplicationException ex = new ApplicationException(member.Error);
                ex.Data.Add("Media", member);
                throw ex;
            }
        }

        public Member GetMemberOnUsername(string _username)
        {
            Member member = this.MemberDAL.GetMemberByUsername(_username);

            if (member.IsValid)
            {
                return member;
            }
            else
            {
                ApplicationException ex = new ApplicationException(member.Error);
                ex.Data.Add("Member", member);
                throw ex;
            }
        }

        public bool IfMemberIsAdmin(string username)
        {
            Member member = this.GetMemberOnUsername(username);
            if (member.Status == "Administrator")
            {
                return true;
            }
            return false;
        }

        internal Media GetMediaByName(string navigatedSeriesName)
        {
            Media media = this.MediaDAL.GetMediaByName(navigatedSeriesName);

            if (media.IsValid)
            {
                return media;
            }
            else
            {
                ApplicationException ex = new ApplicationException(media.Error);
                ex.Data.Add("Media", media);
                throw ex;
            }
        }

        internal int getAmountOfEpisodesBySeries(string series)
        {
            int episodeAmount = this.MediaDAL.GetAmountOfEpisodesBySeries(series);
            if (episodeAmount > 0)
            {
                return episodeAmount;
            }
            else 
            {
                throw new ApplicationException("Amount of episodes are lower then 1");
            }
            
        }


        public List<Genre> GetGenres()
        {
            return this.MediaDAL.GetGenres();
        }

        public List<Videotype> GetVideoTypes()
        {
            return this.MediaDAL.GetVideotypes();
        }


     
    }
}