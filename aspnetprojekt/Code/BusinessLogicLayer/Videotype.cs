﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace aspnetprojekt.Code.BusinessLogicLayer
{
    public class Videotype : BusinessObjectBase
    {
        private string _type;

        public int TypeID
        {
            get;
            set;
        }

        public string Type
        {
            get { return _type; }
            set
            {
                _type = value != null ? value.Trim() : null;
            }
        }

        public Videotype()
        {
        }

        public Videotype(string type)
        {
            Type = type;
        }
        public Videotype(string type, int typeID)
        {
            Type = type;
            TypeID = typeID;
        }
    }
}