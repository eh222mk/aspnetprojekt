﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace aspnetprojekt.Code.BusinessLogicLayer
{
    public class Media : BusinessObjectBase
    {
        private string _name;
        private Genre _genre;
        private Videotype _type;
        private int _episodes;
        private string _summary;

        public int MediaID
        {
            get;
            set;
        }

        public int Episodes
        {
            get
            {
                return this._episodes;
            }
            set
            {
                this.ValidationErrors.Remove("MediaEpisodes");
                if (value < 1 || value == null)
                {
                    this.ValidationErrors.Add("MediaName", "The media must have atleast 1 episode");
                }

                this._episodes = value;
            }
        }

        public string Name
        {
            get
            {
                return this._name;
            }
            set
            {
                this.ValidationErrors.Remove("MediaName");

                if (String.IsNullOrWhiteSpace(value))
                {
                    this.ValidationErrors.Add("MediaName", "No media name");
                }
                else if (value.Length > 30)
                {
                    this.ValidationErrors.Add("MediaName", "Too long media name");
                }

                this._name = value != null ? value.Trim() : null;
            }
        }

        public int GetVideoTypeID
        {
            get { return _type.TypeID; }
            set
            {
                if (_type == null)
                    _type = new Videotype();

                _type.TypeID = value;
            }
        }

        public int GetGenreID
        {
            get
            {
                return Genre.GenreID;
            }
            set
            {
                if (Genre == null)
                    Genre = new Genre();


                Genre.GenreID = value;
            }
        }

        public Genre Genre
        {
            get
            {
                return this._genre;
            }
            set
            {
                _genre = value;
            }
        }

        public Videotype Type
        {
            get
            {
                return this._type;
            }
            set
            {
                this._type = value;
            }
        }

        public string Summary
        {
            get
            {
                return this._summary;
            }
            set
            {
                this.ValidationErrors.Remove("MediaSummary");

                if (String.IsNullOrWhiteSpace(value))
                {
                    this.ValidationErrors.Add("MediaSummary", "No media summary");
                }
                else if (value.Length > 200)
                {
                    this.ValidationErrors.Add("MediaSummary", "Too long media summary");
                }

                this._summary = value != null ? value.Trim() : null;
            }
        }

        public Media()
        {
        }

        public Media(string name, Genre genre, Videotype type, int episodes, string summary)
        {
            this.Name = name;
            this.Genre = genre;
            this.Type = type;
            this.Episodes = episodes;
            this.Summary = summary;
        }

        public Media(string name, Genre genre, Videotype type, int episodes, string summary, int id)
        {
            this.Name = name;
            this.Genre = genre;
            this.Type = type;
            this.Episodes = episodes;
            this.Summary = summary;
            this.MediaID = id;
        }
    }
}