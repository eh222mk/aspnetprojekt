﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.ComponentModel;
using System.Reflection;

namespace aspnetprojekt.Code
{
    public static class PageExtensions
    {
        /// <summary>
        /// Lägger till ett CustomValidator-objekt till samlingen ValidatorCollection.
        /// </summary>
        /// <param name="message">Felmeddelande som ska visas av en ValidationSummary-kontroll.</param>
        /// <param name="validationGroup">Namnet på den valideringsgrupp som ska användas.</param>
        public static void AddErrorMessage(this Page page, string message, string validationGroup)
        {
            var validator = new CustomValidator
            {
                IsValid = false,
                ErrorMessage = message,
                ValidationGroup = validationGroup
            };

            page.Validators.Add(validator);
        }

        /// <summary>
        /// Går igenom samtliga publika egenskaper för obj och undersöker om felmeddelande finns som i så
        /// fall läggs till samlingen ValidatorCollection.
        /// </summary>
        /// <param name="obj">Referens till affärslogikobjekt.</param>
        /// <param name="validationGroup"></param>
        public static void AddErrorMessage(this Page page, IDataErrorInfo obj, string validationGroup)
        {
            // Hämtar och loopar igenom samtliga publika, icke statiska, egenskaper objektet har och filterar ut
            // den egenskaper som har ett felmeddelande associerat med egenskapens namn för att kopiera meddelandet 
            // till samlingen ValidatorCollection.
            obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(p => String.IsNullOrWhiteSpace(obj[p.Name]))
                .Select(p => p.Name)
                .ToList()
                .ForEach(propertyName => PageExtensions.AddErrorMessage(page, obj[propertyName], validationGroup));
        }
    }
}