﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using aspnetprojekt.Code.BusinessLogicLayer;
using System.Data.SqlClient;
using System.Data;

namespace aspnetprojekt.Code.DataAccessLayer
{
    public class MemberDAL : DALBase
    {

        public void RegisterMember(Member member)
        {
            using (SqlConnection conn = CreateConnection())
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("dbo.usp_newUser", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = member.Username;
                    cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = member.Password;
                    cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = member.Name;
                    cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = member.Email;
                    cmd.Parameters.Add("@Nationality", SqlDbType.VarChar).Value = member.Nationality;

                    //cmd.Parameters.Add("@ContactId", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

                    conn.Open();

                    cmd.ExecuteNonQuery();

                    //contact.contactID = (int)cmd.Parameters["@ContactId"].Value;

                }
                catch
                {
                    throw new ApplicationException("An error occured in the data access layer");
                }
            }
        }

        internal Member GetMemberByUsername(string _username)
        {
            using (SqlConnection conn = CreateConnection())
            {
                    SqlCommand cmd = new SqlCommand("dbo.usp_GetMemberByUsername", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Username", _username);

                    conn.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            var MemberIDIndex = reader.GetOrdinal("UserID");
                            var UsernameIndex = reader.GetOrdinal("Username");
                            var PasswordIndex = reader.GetOrdinal("Password");
                            var NameIndex = reader.GetOrdinal("Name");
                            var EmailIndex = reader.GetOrdinal("Email");
                            var NationalityIndex = reader.GetOrdinal("Nationality");
                            var UserStatusIndex = reader.GetOrdinal("UserStatus");

                            return new Member
                            {
                                MemberID = reader.GetInt32(MemberIDIndex),
                                Username = reader.GetString(UsernameIndex),
                                Password = reader.GetString(PasswordIndex),
                                Name = reader.GetString(NameIndex),
                                Email = reader.GetString(EmailIndex),
                                Nationality = reader.GetString(NationalityIndex),
                                Status = reader.GetString(UserStatusIndex)
                            };
                        }
                    }
                    return null;
            }
        }

        internal List<Member> GetMemberList()
        {
            List<Member> memberList = new List<Member>();
            using (SqlConnection conn = CreateConnection())
            {

                SqlCommand cmd = new SqlCommand("dbo.usp_ShowUsers", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                conn.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        var MemberIDIndex = reader.GetOrdinal("UserID");
                        var UsernameIndex = reader.GetOrdinal("Username");
                        var PasswordIndex = reader.GetOrdinal("Password");
                        var NameIndex = reader.GetOrdinal("Name");
                        var EmailIndex = reader.GetOrdinal("Email");
                        var NationalityIndex = reader.GetOrdinal("Nationality");
                        var UserStatusIndex = reader.GetOrdinal("UserStatus");

                        while (reader.Read())
                        {
                             memberList.Add(new Member
                             {
                                 MemberID = reader.GetInt32(MemberIDIndex),
                                 Username = reader.GetString(UsernameIndex),
                                 Password = reader.GetString(PasswordIndex),
                                 Name = reader.GetString(NameIndex),
                                 Email = reader.GetString(EmailIndex),
                                 Nationality = reader.GetString(NationalityIndex),
                                 Status = reader.GetString(UserStatusIndex)
                             });
                        }
                    }
                }
                return memberList;
            }
        }

        internal void DeleteMember(Member member)
        {
            using (SqlConnection conn = CreateConnection())
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("dbo.usp_DelUser", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = member.MemberID;

                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
                catch
                {
                    throw new ApplicationException("An error occured in the data access layer");
                }
            }
        }

        internal void UpdateMemberByMember(Member member)
        {
            using (SqlConnection conn = CreateConnection())
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("dbo.usp_changeUser", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = member.MemberID;
                    cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = member.Username;
                    cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = member.Name;
                    cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = member.Email;
                    cmd.Parameters.Add("@Nationality", SqlDbType.VarChar).Value = member.Nationality;

                    conn.Open();

                    cmd.ExecuteNonQuery();
                }
                catch
                {
                    throw new ApplicationException("An error occured in the data access layer");
                }
            }
        }
    }
}