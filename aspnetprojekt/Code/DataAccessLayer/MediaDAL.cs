﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using aspnetprojekt.Code.BusinessLogicLayer;

namespace aspnetprojekt.Code.DataAccessLayer
{
    public class MediaDAL : DALBase
    {
        public List<Media> GetMediaList()
        {
            List<Media> mediaList = new List<Media>();
            using (SqlConnection conn = CreateConnection())
            {

                SqlCommand cmd = new SqlCommand("dbo.usp_ShowVideos", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                conn.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        var MemberIDIndex = reader.GetOrdinal("VideoID");
                        var NameIndex = reader.GetOrdinal("Name");
                        var TypeIndex = reader.GetOrdinal("VideoType");
                        var TypeIdIndex = reader.GetOrdinal("TypeID");
                        var GenreIndex = reader.GetOrdinal("Genre");
                        var GenreIdIndex = reader.GetOrdinal("GenreID");
                        var EpisodesIndex = reader.GetOrdinal("Episodes");
                        var SummaryIndex = reader.GetOrdinal("Summary");

                        while (reader.Read())
                        {
                            try
                            {
                                mediaList.Add(new Media
                                {
                                    MediaID = reader.GetInt32(MemberIDIndex),
                                    Name = reader.GetString(NameIndex),
                                    Type =  new Videotype(reader.GetString(TypeIndex), reader.GetInt32(TypeIdIndex)),
                                    Genre = new Genre(reader.GetString(GenreIndex), reader.GetInt32(GenreIdIndex)),
                                    Episodes = reader.GetInt32(EpisodesIndex),
                                    Summary = reader.GetString(SummaryIndex)
                                });
                            }
                            catch
                            {
                                mediaList.Add(new Media
                                {
                                    MediaID = reader.GetInt32(MemberIDIndex),
                                    Name = reader.GetString(NameIndex),
                                    Type = new Videotype(reader.GetString(TypeIndex)),
                                    Genre = new Genre(reader.GetString(GenreIndex)),
                                    Episodes = reader.GetInt32(EpisodesIndex)
                                });
                            }

                        }
                    }
                }
                return mediaList;
            }
        }

        internal Media GetMediaByName(string _name)
        {
            try
            {
                using (SqlConnection conn = CreateConnection())
                {
                    SqlCommand cmd = new SqlCommand("dbo.usp_ShowVideoByName", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Name", _name);

                    conn.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            var VideoIdIndex = reader.GetOrdinal("VideoID");
                            var NameIndex = reader.GetOrdinal("Name");
                            var TypeIndex = reader.GetOrdinal("VideoType");
                            var GenreIndex = reader.GetOrdinal("Genre");
                            var EpisodeIndex = reader.GetOrdinal("Episodes");
                            var SummaryIndex = reader.GetOrdinal("Summary");

                            string summary = null;
                            try
                            {
                                summary = reader.GetString(SummaryIndex);
                            }
                            catch { }

                            if (summary != null)
                            {
                                return new Media
                                {
                                    MediaID = reader.GetInt32(VideoIdIndex),
                                    Name = reader.GetString(NameIndex),
                                    Type = new Videotype(reader.GetString(TypeIndex)),
                                    Genre = new Genre(reader.GetString(GenreIndex)),
                                    Episodes = reader.GetInt32(EpisodeIndex),
                                    Summary = summary
                                };
                            }
                            else
                            {
                                return new Media
                                {
                                    MediaID = reader.GetInt32(VideoIdIndex),
                                    Name = reader.GetString(NameIndex),
                                    Type = new Videotype(reader.GetString(TypeIndex)),
                                    Genre = new Genre(reader.GetString(GenreIndex)),
                                    Episodes = reader.GetInt32(EpisodeIndex)
                                };
                            }
                        }
                    }
                    return null;
                }
            }
            catch
            {
                throw new Exception("An error occured in the data access layer");
            }
        }

        internal int GetAmountOfEpisodesBySeries(string series)
        {
            using (SqlConnection conn = CreateConnection())
            {
                SqlCommand cmd = new SqlCommand("dbo.usp_GetEpisodesBySeries", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Series", SqlDbType.VarChar).Value = series;

                conn.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        var VideoIdIndex = reader.GetOrdinal("Episodes");
                        return reader.GetInt32(VideoIdIndex);
                    }
                }
            }
            return -1;
        }



        internal void DeleteMediaByMedia(Media media)
        {
            using (SqlConnection conn = CreateConnection())
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("dbo.usp_DelVideo", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@VideoID", SqlDbType.Int).Value = media.MediaID;

                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
                catch
                {
                    throw new ApplicationException("An error occured in the data access layer");
                }
            }
        }


        internal void UpdateMediaByMedia(Media media)
        {
            using (SqlConnection conn = CreateConnection())
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("dbo.usp_changeVideo", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@VideoID", SqlDbType.Int).Value = media.MediaID;
                    cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = media.Name;
                    cmd.Parameters.Add("@GenreID", SqlDbType.VarChar).Value = media.Genre.GenreID;
                    cmd.Parameters.Add("@TypeID", SqlDbType.VarChar).Value = media.Type.TypeID;
                    cmd.Parameters.Add("@Episodes", SqlDbType.Int).Value = media.Episodes;

                    if (media.Summary != null)
                    {
                        cmd.Parameters.Add("@Summary", SqlDbType.VarChar).Value = media.Summary;
                    }
                    conn.Open();

                    cmd.ExecuteNonQuery();
                }
                catch
                {
                    throw new ApplicationException("An error occured in the data access layer");
                }
            }
        }

        internal void AddMediaByMedia(Media media)
        {
            using (SqlConnection conn = CreateConnection())
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("dbo.usp_newVideo1", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = media.Name;
                    cmd.Parameters.Add("@GenreID", SqlDbType.Int).Value = media.Genre.GenreID;
                    cmd.Parameters.Add("@TypeID", SqlDbType.VarChar).Value = media.Type.TypeID;
                    cmd.Parameters.Add("@Episodes", SqlDbType.Int).Value = media.Episodes;
                    
                    if (media.Summary != null)
                    {
                        cmd.Parameters.Add("@Summary", SqlDbType.VarChar).Value = media.Summary;
                    }
                    conn.Open();

                    cmd.ExecuteNonQuery();
                }
                catch
                {
                    throw new ApplicationException("An error occured in the data access layer");
                }
            }
        }

        private int GetGenreIdByName(string name)
        {
            using (SqlConnection conn = CreateConnection())
            {
                SqlCommand cmd = new SqlCommand("dbo.[usp_GetGenreIdByGenre]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Genre", SqlDbType.VarChar).Value = name;

                conn.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        var VideoIdIndex = reader.GetOrdinal("GenreID");
                        return reader.GetInt32(VideoIdIndex);
                    }
                }
            }
            return 1;
        }

        private int GetTypeIdByName(string name)
        {
            using (SqlConnection conn = CreateConnection())
            {
                SqlCommand cmd = new SqlCommand("dbo.[usp_GetTypeIdByType]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@VideoType", SqlDbType.VarChar).Value = name;

                conn.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        var VideoIdIndex = reader.GetOrdinal("TypeID");
                        return reader.GetInt32(VideoIdIndex);
                    }
                }
            }
            return 3;
        }

        internal List<Genre> GetGenres()
        {
            using (SqlConnection conn = CreateConnection())
            {
                List<Genre> genreList = new List<Genre>();
                SqlCommand cmd = new SqlCommand("dbo.[usp_ShowGenre]", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                conn.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    var GenreIDIndex = reader.GetOrdinal("GenreID");
                    var GenreIndex = reader.GetOrdinal("Genre");

                    while (reader.Read())
                    {
                        genreList.Add(new Genre
                        {
                            GenreID = reader.GetInt32(GenreIDIndex),
                            GetGenre = reader.GetString(GenreIndex)
                        });
                    }
                }
                return genreList;
            }
        }

        internal List<Videotype> GetVideotypes()
        {
            using (SqlConnection conn = CreateConnection())
            {
                List<Videotype> typeList = new List<Videotype>();
                SqlCommand cmd = new SqlCommand("dbo.[usp_ShowVideoType]", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                conn.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    var TypeIDIndex = reader.GetOrdinal("TypeID");
                    var TypeIndex = reader.GetOrdinal("VideoType");

                    while (reader.Read())
                    {
                        typeList.Add(new Videotype
                        {
                            TypeID = reader.GetInt32(TypeIDIndex),
                            Type = reader.GetString(TypeIndex)
                        });
                    }
                }
                return typeList;
            }
        }

    }
}