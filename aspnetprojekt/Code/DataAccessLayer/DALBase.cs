﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace aspnetprojekt.Code.DataAccessLayer
{
    public class DALBase
    {
        private string _connectionString;

        protected SqlConnection CreateConnection()
        {
            return new SqlConnection(_connectionString);
        }

        public DALBase()
        {
            _connectionString = WebConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
        }
    }
}