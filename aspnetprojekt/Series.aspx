﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Series.aspx.cs" Inherits="aspnetprojekt.Series" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<asp:Label runat="server" ID="TitleLabel" Visible="false"></asp:Label>

    <div id="SeriesSummaryDiv">
        <p>
            Summary:
            <asp:Label runat="server" ID="SummaryLabel" Visible="false"></asp:Label>
        </p>
        <p>
            Genre:
            <asp:Label runat="server" ID="GenreLabel" Visible="false"></asp:Label>
        </p>
        <p>
            Type:
            <asp:Label runat="server" ID="TypeLabel" Visible="false"></asp:Label>
        </p>
    </div>
    <div id="SeriesEpisodeListDiv">
        <h2>Episodes:</h2>
        <span id="episodeSpan">
        
        </span>
    </div>
</asp:Content>
