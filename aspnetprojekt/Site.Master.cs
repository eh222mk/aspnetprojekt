﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using aspnetprojekt.Code.BusinessLogicLayer;

namespace aspnetprojekt
{
    public partial class Site : System.Web.UI.MasterPage
    {
        private const string SessionUsername = "Username";
        private Service service = new Service();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IfAdmin())
            {
                AdministrateLink.Enabled = true;
                AdministrateLink.Visible = true;
            }
            else
            {
                AdministrateLink.Enabled = false;
                AdministrateLink.Visible = false;
            }

            try
            {
                if (Session[SessionUsername].ToString() == null)
                {
                    LogoutButton.Visible = false;
                }
                else
                {
                    LoginButton.Visible = false;
                    RegisterButton.Visible = false;
                }
            }
            catch { LogoutButton.Visible = false; LoginButton.Visible = true; RegisterButton.Visible = true; }
        }

        protected void LogoutLinkButton_Click(object sender, EventArgs e)
        {
            this.ClearSession();
            Response.Redirect("~/Default.aspx");
        }

        internal Member LoginWithUserInput(string _username, string _password)
        {
            Member member = service.GetMemberOnUsername(_username);

            if (member != null)
            {
                if (_password == member.Password)
                {
                    return member;
                }
            }
            return null;
        }

        internal bool IfAdmin()
        {
            try
            {
                if (Session[SessionUsername].ToString() != null)
                {
                    if (this.service.IfMemberIsAdmin(this.GetSessionUsername()))
                    {
                        return true;
                    }
                }
                return false;
            }
            catch
            {
                return false;
            }
            
        }

        internal void ClearSession()
        {
            Session[SessionUsername] = null;
        }

        internal string GetSessionUsername()
        {
            return Session[SessionUsername].ToString();
        }

        internal void SetSession(Member member)
        {
            Session[SessionUsername] = member.Username;
        }
    }
}