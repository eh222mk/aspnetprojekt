﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="aspnetprojekt.Register" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <asp:ValidationSummary ID="ValidationSummary1" CssClass="ValidationErrors" runat="server" ValidationGroup="RegisterValidation"/>
    <div id="RegisterForm">
        <h2>Register</h2>
        <asp:Label runat="server" id="RegisterCompletedLabel" ForeColor="Green"></asp:Label>
        
        <p>
            Username:
            <asp:TextBox runat="server" ID="UsernameTextBox" CssClass="RegisterTextBoxes"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="ValidationErrors" runat="server" ErrorMessage="You must type in a username!"
            ControlToValidate="UsernameTextBox" ValidationGroup="RegisterValidation">*</asp:RequiredFieldValidator>
        </p>
        <p>
            Password:
            <asp:TextBox runat="server" ID="PasswordTextBox" CssClass="RegisterTextBoxes" TextMode="Password"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="ValidationErrors" runat="server" ErrorMessage="You must type in a password!"
            ControlToValidate="PasswordTextBox" ValidationGroup="RegisterValidation">*</asp:RequiredFieldValidator>
        </p>
        <p>
            Repeat Password:
            <asp:TextBox runat="server" ID="RepeatPasswordTextBox" CssClass="RegisterTextBoxes" TextMode="Password"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RepeatPasswordRequiredFieldValidator" CssClass="ValidationErrors" runat="server" ErrorMessage="You must repeat your password!"
            ControlToValidate="RepeatPasswordTextBox" ValidationGroup="RegisterValidation">*</asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" ID="RepeatPasswordCompateValidator" CssClass="ValidationErrors" ErrorMessage="The passwords doesn't match!"
            ControlToValidate="RepeatPasswordTextBox" ControlToCompare="PasswordTextBox" ValidationGroup="RegisterValidation">*</asp:CompareValidator>
        </p>
        <p>
            Full name:
            <asp:TextBox runat="server" ID="FullNameTextBox" CssClass="RegisterTextBoxes"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" CssClass="ValidationErrors" runat="server" ErrorMessage="You must type in a name"
            ControlToValidate="FullNameTextBox" ValidationGroup="RegisterValidation">*</asp:RequiredFieldValidator>
        </p>
        <p>
            Email:
            <asp:TextBox runat="server" ID="EmailTextBox" CssClass="RegisterEmailTextBox"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" CssClass="ValidationErrors" ErrorMessage="You must type in an email"
            ControlToValidate="EmailTextBox" ValidationGroup="RegisterValidation">*</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" CssClass="ValidationErrors" runat="server" ErrorMessage="That is not a valid email"
            ControlToValidate="EmailTextBox" ValidationGroup="RegisterValidation" ValidationExpression="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$"
            >*</asp:RegularExpressionValidator>
        </p>
        <p>
            Nationality:
            <asp:TextBox runat="server" ID="NationalityTextBox" CssClass="RegisterTextBoxes"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" CssClass="ValidationErrors" runat="server" ErrorMessage="You must type in a nationality"
            ControlToValidate="NationalityTextBox" ValidationGroup="RegisterValidation">*</asp:RequiredFieldValidator>
        </p>
        <asp:Button runat="server" ID="RegisterButton" OnClick="RegisterButton_Click" Text="Register" CausesValidation="true" ValidationGroup="RegisterValidation"/>
    </div>

</asp:Content>
