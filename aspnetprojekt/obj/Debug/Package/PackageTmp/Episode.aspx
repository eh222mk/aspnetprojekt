﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Episode.aspx.cs" Inherits="aspnetprojekt.Episode" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<asp:Label ID="HeaderLabel" CssClass="EpisodeHeaderLabel" runat="server"></asp:Label>

<asp:Panel runat="server" ID="Panel1" Visible="false">
        <asp:label runat="server" Text="As a none registered user you are only allowed to watch up to episode 3."></asp:label>
    <p>
        <asp:HyperLink NavigateUrl="~/Register.aspx" Text=" Please register to watch more videos on this series." runat="server" ID="PleaseRegisterLink"></asp:HyperLink>
    </p>
</asp:Panel>

<div id="EpisodeContent">
<asp:Panel runat="server" ID="Panel2" Visible="true">
    <div id="EpisodeVideo">
        <video height="360" width="600" controls="controls" autoplay="autoplay" src="Content/bbb_trailer_iphone270P.m4v"></video>
    </div>
</asp:Panel>
    <div id="EpisodeHyperLinks">
        <asp:HyperLink runat="server" ID="BackOneEpisode"></asp:HyperLink>
        <asp:HyperLink runat="server" ID="BackToSeries"></asp:HyperLink>
        <asp:HyperLink runat="server" ID="ForwardOneEpisode"></asp:HyperLink>
    </div>
</div>
</asp:Content>
