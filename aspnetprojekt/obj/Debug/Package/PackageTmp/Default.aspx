﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="aspnetprojekt.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<asp:Objectdatasource ID="SeriesListObjectDataSource" runat="server" SelectMethod="GetSeriesList"
TypeName="aspnetprojekt.Code.BusinessLogicLayer.Service" DataObjectTypeName="Code.BusinessLogicLayer.Media" />

<asp:ListView runat="server" ID="SeriesListView" DataSourceID="SeriesListObjectDataSource" DataKeyNames="MediaID">
    <LayoutTemplate>
        <table>
            <tr>
                <th>
                    Name
                </th>
                <th>
                    Genre
                </th>
                <th>
                    Type
                </th>
            </tr>
            <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
        </table>
    </LayoutTemplate>
    <ItemTemplate>
        <tr>
            <td>
                <asp:HyperLink ID="MediaNameLink"  runat="server" Text='<%# Bind("Name") %>' CausesValidation="false" 
                NavigateUrl='<%# Eval("Name", "~/Series.aspx?series={0}") %>'></asp:HyperLink>
            </td>
            <td>
                <asp:HyperLink ID="MediaGenreLink"  runat="server" Text='<%# Bind("Genre.GetGenre") %>' CausesValidation="false"></asp:HyperLink>
            </td>
            <td>
                <asp:HyperLink ID="MediaTypeLink"  runat="server" Text='<%# Bind("Type.Type") %>' CausesValidation="false"></asp:HyperLink>
            </td>
        </tr>
    </ItemTemplate>
    <EmptyDataTemplate>
        <table class="grid">
            <tr>
                <td>
                    Information about media is missing.
                </td>
            </tr>
        </table>
    </EmptyDataTemplate>
</asp:ListView>

</asp:Content>
