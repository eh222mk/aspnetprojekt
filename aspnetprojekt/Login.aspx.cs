﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using aspnetprojekt.Code.BusinessLogicLayer;
using aspnetprojekt.Code;

namespace aspnetprojekt
{
    public partial class Login : System.Web.UI.Page
    {
        private string ValidationGroup = "LoginValidation";

        public new Site site { get { return base.Master as Site; } }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void LoginButton_Click(object sender, EventArgs e)
        {
            try
            {
                string username = UserTextBox.Text;
                string password = PasswordTextBox.Text;

                Member member = new Member();
                member.Username = username;
                member.Password = password;

                if(member.IsValid)
                {
                    Member loggedInMember = site.LoginWithUserInput(username, password);
                
                    if (loggedInMember != null && loggedInMember.IsValid)
                    {
                        var ID = Convert.ToInt64(loggedInMember.MemberID);

                        site.SetSession(loggedInMember);

                        Response.Redirect("~/Default.aspx");
                    }
                    else
                    {
                        PageExtensions.AddErrorMessage(this, "Wrong username and/or password", "loginvalidation");
                    }
                }
            }
            catch
            {
                PageExtensions.AddErrorMessage(this, "An error has occured", "loginvalidation");
            }
        }
    }
}